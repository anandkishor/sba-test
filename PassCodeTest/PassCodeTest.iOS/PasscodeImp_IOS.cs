﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PassCodeTest;
using Foundation;
using UIKit;
using Xamarin.Forms;
using PassCodeTest.Helper;
using Security;
using NetworkExtension;

[assembly: Xamarin.Forms.Dependency(typeof(PassCodeTest.iOS.PasscodeImp_IOS))]
namespace PassCodeTest.iOS
{

    public class PasscodeImp_IOS : IPassCodeTest
    {
        public bool IstherePassCode()
        {
           // NEHotspotConfigurationManager

            var secRecord = new SecRecord(SecKind.GenericPassword)
            {
                Label = "Check if passcode is set",
                Description = "Check if passcode is set",
                Account = "Check if passcode is set",
                Service = "Check if passcode is set",
                Comment = "Check if passcode is set",
                ValueData = NSData.FromString("Check if passcode is set"),
                Generic = NSData.FromString("Check if passcode is set")
            };
            SecKeyChain.Remove(secRecord);
            secRecord.AccessControl = new SecAccessControl(SecAccessible.WhenPasscodeSetThisDeviceOnly);
            //secRecord.AccessControl = new SecAccessControl(SecAccessible.WhenPasscodeSetThisDeviceOnly, SecAccessControlCreateFlags.UserPresence);
            var status = SecKeyChain.Add(secRecord);
            if (SecStatusCode.Success == status)
            {
                SecKeyChain.Remove(secRecord);
                //SecKeyChain.Add(secRecord);
                return true;
            }
            return false;
        }
    }
}