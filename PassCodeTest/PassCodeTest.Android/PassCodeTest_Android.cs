﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using PassCodeTest.Helper;
using Android.Security;

[assembly: Dependency(typeof(PassCodeTest.Droid.PassCodeTest_Android))]
namespace PassCodeTest.Droid
{
    public class PassCodeTest_Android : IPassCodeTest
    {
        public bool IstherePassCode()
        {

            Android.Content.Context context = Android.App.Application.Context;
            //ContentResolver cr = context.ContentResolver;
            //try
            //{

            //    int lockPatternEnable = Settings.Secure.getInt(cr, Settings.Secure.LOCK_PATTERN_ENABLED);
            //    return lockPatternEnable == 1;
            //}
            //catch (Settings.SettingNotFoundException e)
            //{
            //    return false;
            //}

            KeyguardManager keyguardManager = (KeyguardManager)context.GetSystemService(Context.KeyguardService); //api 16+
            return keyguardManager.IsKeyguardSecure;
        }
    }
}