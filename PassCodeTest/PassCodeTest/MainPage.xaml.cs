﻿using PassCodeTest.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace PassCodeTest
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            bool rtnval = DependencyService.Get<IPassCodeTest>().IstherePassCode();
            lblPasscodeThere.Text = "PassCode Available :" + rtnval.ToString();
        }
    }
}
